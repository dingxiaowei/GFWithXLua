﻿using ProtoBuf;

namespace DialGame
{
    [ProtoContract]
    public class CSPacketHeader : PacketHeaderBase
    {
        public CSPacketHeader(int packetId)
            : base(PacketType.ClientToServer, packetId)
        {

        }
    }
}
