﻿
namespace UnityGameFramework.Editor.AssetBundleTools
{
    internal enum AssetSorterType
    {
        Path,
        Name,
        Guid,
    }
}
